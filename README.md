SCS Events File to POR Conversion Tool
==========================================

This directory contains a script that makes use of a XLST to convert
the output of the Euclid SOC SCS subsystem into POR format, to be
transferred to MOC.

To get some help on the usage of the tool, just type in:

    $ ./SCSEvts2POR.sh -h

The tool uses `xsltproc` and `xmllint`, so you must be sure that
your system has these tools installed.
