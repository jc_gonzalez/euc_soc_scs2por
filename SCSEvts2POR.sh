#!/bin/bash
##############################################################################
# File       : build.sh - Compiles and installs the application
# Domain     : SPF.scripts
# Version    : 3.0
# Date       : 2020/05/08
# Copyright (C) 2015-2020 J C Gonzalez
#_____________________________________________________________________________
# Purpose    : Compiles and installs the application
# Created by : J C Gonzalez
# Status     : Prototype
# History    : See <Changelog>
###############################################################################

#- Script variables

SCRIPTPATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
SCRIPTNAME=$(basename "${BASH_SOURCE[0]}")
CURDIR=$(pwd)

SCS2PORxslt="${SCRIPTPATH}/scs_evt_2_por.xslt"

#- Options
QUIET="no"
OPTS="--timing"
PARAMS=""

#- Other
DATE=$(date +"%Y%m%d%H%M%S")

###### Handy functions

usage () {
    local opts="[ -h ] [ -o <outputFileName> ] [ -s <seqItem> ] [ -q ] <inputFileName> "
    echo "Usage: ${SCRIPTNAME}  $opts"
    echo "where:"
    echo "  -h         Show this usage message"
    echo "  -o         Used to specify the file name of the resulting XML File"
    echo "  -s         Used to specify the sequence item name to use for the conversion for POR sequences"
    echo "             (default:Sequence)"
    echo "  -q         Quiet operation"
    echo ""
    exit "$1"
}

#### Parse command line and display grettings

## Parse command line
while getopts :ho:s:q OPT; do
    case $OPT in
        h) usage 1 ;;
        q) QUIET="yes"; OPTS="" ;;
        o) OUTPUT_USER_FILE="${OPTARG}" ;;
        s) PARAMS="--stringparam sequence_name ${OPTARG}" ;;
        *) usage 2 ;;
    esac
done
shift $((OPTIND - 1))
OPTIND=1

if [ $# -lt 1 ]; then
    usage 2
fi

INPUT_FILE="$1"
OUTPUT_FILE="${OUTPUT_USER_FILE:-$(basename "${INPUT_FILE}" .xml).por.xml}"

if [ "${QUIET}" = "no" ]; then
    echo "============================================================"
    echo "${SCRIPTNAME} - Convert SCS Events file to POR file"
    echo "${DATE}"
    echo "============================================================"
fi

xsltproc ${OPTS} ${PARAMS} "${SCS2PORxslt}" "${INPUT_FILE}" 2>/tmp/$$.txt | \
    xmllint --format - > "${OUTPUT_FILE}"

cat /tmp/$$.txt
rm /tmp/$$.txt

cd "${CURDIR}"
exit 0
